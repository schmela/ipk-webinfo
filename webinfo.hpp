#include <iostream>
#include <string>

using namespace std;

class TParams
{
	private:
		string object; // adresa pozadovaneho objektu
		int actions[4]; 

		void setAction(int type, int index);
		void setObject(string path);
	public:
		TParams(int argc,char *argv[]);
		int getAction(int index);
		string getObject();
		
		class badParameters{};
		class badActionIndex{};
};

class THead
{
  private:
    string head;
	int port;
	string hostName;
	string file;
	int responseCode;
	string responseMessage;

    void setHead(string aHead);
	void setHostName(string aHostName);
	void setPort(int aPort);
	void setFile(string aFile);
	void setResponseCode(int aCode);
	void setResponseMessage(string aMessage);
	
  public:
    THead(string aObject);
    void printFullHead();
    void printObjectLength();
    void printServerIndentification();
    void printLastModification();
    void printObjectType();
    
    string getHead();
	string getHostName();
	int getPort();
	string getFile();
	int getResponseCode();
	string getResponseMessage();
    
	class badHostName{};
    class socketError{};
    class gethostbynameError{};
    class connectError{};
    class writeError{};
    class readError{};
    class closeError{};        
    class noResponseNumberError{};        
    class noResponseMessageError{};        
};

string regexp(string source, string pattern);
class regexpCompilationError{};
