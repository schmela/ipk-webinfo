#include <iostream>
#include <string>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <regex.h>
#include "webinfo.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int MIN_INDEX = 0; //minimalni index akce
const int MAX_INDEX = 3; //maximalni index akce (jsou celkem 4 akce -l -s -m -t)

const int UNDEFINED = -1;
const int FULLHEAD = 0;
const int OBJECT_LENGTH = 1; // -l delka obektu
const int SERVER_IDENTIFICATION = 2; // -s identifikace serveru
const int LAST_MODIFICATION = 3; // -m informace o posledni modifikaci serveru
const int OBJECT_TYPE = 4; // -t typ obsahu objektu

using namespace std;

TParams::TParams(int argc,char *argv[])
{
	for (int i=MIN_INDEX; i<=MAX_INDEX; i++) {setAction(UNDEFINED,i);} 
	
	int action_index=MIN_INDEX;
	if (argc==1) {throw badParameters();}
	else if (argc==2) {
		if (strcmp (argv[1],"-h") == 0)
		{
			cout <<	"WEBINFO - program pro vypis informaci o obektu na webu"<<endl<<
					"pouziti:"<<endl<<
					"webinfo -h vypise tuto napovedu"<<endl<<
					"webinfo [prepinace] [adresa obektu] - vypise kompletni hlavicku o zadanem obektu"<<endl<<
					"-l vypise delku obektu"<<endl<<
					"-s vypise identifikaci serveru"<<endl<<
					"-m vypise info o posledni modifikaci obektu"<<endl<<
					"-t vypise typ obsahu obektu"<<endl<<flush;
			exit(EXIT_SUCCESS);
		}
		else
		{
			setAction(FULLHEAD,action_index);
			setObject(argv[1]);
		}
	} 
	else {
		setObject(argv[argc-1]);
		int l=0,s=0,m=0,t=0; // pocty kolikrat ktery argumemt je
		for (int i=1; i<argc-1; i++)
		{
			if ((argv[i][0]!='-')||(strlen(argv[i])==1)) {throw badParameters();}
			
			// zjistovani co se bude delat v jakem poradi (ktery prepinac bude kolikaty)
			for (unsigned int j=1; j<strlen(argv[i]); j++)
			{
				if (argv[i][j]=='l')
					{ if (l++==0) {setAction(OBJECT_LENGTH,action_index++);}}
				else if (argv[i][j]=='s') 
					{ if (s++==0) {setAction(SERVER_IDENTIFICATION,action_index++);}}
				else if (argv[i][j]=='m') 
					{ if (m++==0) {setAction(LAST_MODIFICATION,action_index++);}}
				else if (argv[i][j]=='t')
					{ if (t++==0) {setAction(OBJECT_TYPE,action_index++);}}
				else 
					{throw badParameters();}
			}
		}		
	}
}

void TParams::setAction(int type, int index)
{
	if ((index<MIN_INDEX) || (index>MAX_INDEX)) {throw badActionIndex();}
	actions[index]=type;
}

void TParams::setObject(string path)
{
	object = path;
}

int TParams::getAction(int index)
{
	if ((index<MIN_INDEX) || (index>MAX_INDEX)) {throw badActionIndex();}
	return actions[index];
}

string TParams::getObject()
{
	return object;
}

THead::THead (string object)
{
	string adress,port_,file_;
	// ziskani samotne adresy
	if ( (adress = regexp(object,"^http[:]//([^/]+)")) != "")
	{
		// kdyz je v adrese i port, tak se vezme z ni, kdyz ne, da se 80
		if ( (port_ = regexp(adress,".:([[:digit:]]+)")) != "")
		{
			setPort(atoi(port_.c_str()));
			setHostName(regexp(adress,"(.+):[[:digit:]]+"));
		}
		else 
		{
			setPort(80);
			setHostName(adress);
		}
	}
	else
	{
		throw badHostName();
	}

	// ziskani casti adresy za 1. lomitkem
	if ( (file_ = regexp(object,"^http[:]//[^/]+(/.*)")) != "")
	{
		//cout << "&"<<file_<<"&"<<endl;
		setFile(file_);
	}
	else {throw badHostName();}

	// vytvoreni adresy ze jmena hosta
	struct sockaddr_in out_addr;
	bzero (&out_addr , sizeof(out_addr));
	out_addr.sin_family = PF_INET;
	struct hostent *hostent_;
	if ((hostent_ = gethostbyname(getHostName().c_str())) == NULL)
		{throw gethostbynameError();}
	memcpy( &out_addr.sin_addr, hostent_->h_addr, hostent_->h_length);
	out_addr.sin_port = htons(getPort());

	// stvoreni socketu 
	int sockfd;
	if (( sockfd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		{throw socketError();}
	  
	// pripojeni k serveru 
	if ( connect(sockfd, (struct sockaddr*)&out_addr, sizeof(out_addr)) < 0)
		{throw connectError();}
	  
	// poslani pozadavku
	string request_ = "HEAD "+getFile()+" HTTP/1.1\r\nHost: "+getHostName()+"\r\n\r\n";
	if ( write(sockfd, request_.c_str(),request_.size()) < 0)
		{throw writeError();}
  
	// prijmuti zpravy 		  
	char buffer2[4096]={0};
	if ( read(sockfd, buffer2, 4096) < 0 )
		{throw readError();}
	
	// ulozeni hlavicky
	setHead((string)buffer2);
	
	// zavreni spojeni
	if ( close(sockfd) < 0 )
		{throw closeError();}
	
	//cout << getHead()<<endl;
	//cout << regexp(getHead(),"(e>[[:digit:]]{3})");
	// vysekani navratoveho kodu z odpovedi
	string code_;
	//if ( (code_ = regexp(getHead(),"HTTP/1.1 ([[:digit:]]{3}) .+\n")) != "")
	if ( (code_ = regexp(getHead(),"HTTP/1.1 ([[:digit:]]{3}) [^\n]+\n")) != "")
	{
		setResponseCode(atoi(code_.c_str()));
	}
	else {throw noResponseNumberError();}

	string message_;
	if ( (message_ = regexp(getHead(),"HTTP/1.1 [[:digit:]]{3} ([^\n]+)\n")) != "")
	{
		setResponseMessage(message_);
	}
	else {throw noResponseMessageError();}
}

void THead::setHead(string aHead)
{
	head = aHead;
}

void THead::setHostName(string aHostName)
{
	hostName = aHostName;
}

void THead::setPort(int aPort)
{
	port = aPort;
}

void THead::setFile(string aFile)
{
	file = aFile;
}

void THead::setResponseCode(int aCode)
{
	responseCode = aCode;
}

void THead::setResponseMessage(string aMessage)
{
	responseMessage = aMessage;
}

void THead::printFullHead()
{
  cout << head;
}

void THead::printObjectLength()
{
  	string delka = regexp(getHead(),"(Content-Length:[^\n]+)");
  	if (delka != "")
  	{
		cout << delka << endl;
	}
	else
	{
		cout << "Content-Length: N/A" << endl;
	}
}

void THead::printServerIndentification()
{
  	string pom = regexp(getHead(),"(Server:[^\n]+)");
  	if (pom != "")
  	{
		cout << pom << endl;
	}
	else
	{
		cout << "Server: N/A" << endl;
	}
}

void THead::printLastModification()
{
  	string pom = regexp(getHead(),"(Last-Modified:[^\n]+)");
  	if (pom != "")
  	{
		cout << pom << endl;
	}
	else
	{
		cout << "Last-Modified: N/A" << endl;
	}
}

void THead::printObjectType()
{
  	string pom = regexp(getHead(),"(Content-Type:[^\n]+)");
  	if (pom != "")
  	{
		cout << pom << endl;
	}
	else
	{
		cout << "Content-Type: N/A" << endl;
	}
}

string THead::getHead()
{
	return head;
}

string THead::getHostName()
{
	return hostName;
}

int THead::getPort()
{
	return port;
}

string THead::getFile()
{
	return file;
}

int THead::getResponseCode()
{
	return responseCode;
}

string THead::getResponseMessage()
{
	return responseMessage;
}

// vrati obsah prvni zavorky z regexpu, kdyz neni nalezena, vraci se ""
string regexp(string source, string pattern)
{
    regex_t regex;
	int c;

	// kompilace regulerniho vyrazu
	try
	{
    	c = regcomp(&regex, pattern.c_str(), REG_EXTENDED);
		if ( c != 0 ) {throw regexpCompilationError();}
	}
	catch (regexpCompilationError) 
	{
		cerr << "Regex compilation Error" << endl;
		regfree(&regex);
		exit ( EXIT_FAILURE );
	}

	// vykonani regulerniho vyrazu
	size_t     nmatch = 2;
	regmatch_t pmatch[2];
	if ((regexec(&regex,source.c_str(),nmatch,pmatch,0)) == 0)
	{
		int begin = (int)pmatch[1].rm_so;
		int end = (int)pmatch[1].rm_eo;
		int length = end-begin;
		regfree(&regex);
		if ((begin==-1)||(end==-1)) return "";
		else return source.substr(begin,length); 
	}
	else
	{
		regfree(&regex);
		return "";
	}
}

int main(int argc,char *argv[]) 
{
	try
	{
		TParams parametry(argc,argv);
		THead *hlavicka = new THead(parametry.getObject());
		int pocet_redirectu = 0;
		while ( ( hlavicka->getResponseCode() < 200 ) || (hlavicka->getResponseCode() > 206) )
		{
			if ((hlavicka->getResponseCode()>=400)&&(hlavicka->getResponseCode()<600))
			{
				cerr << hlavicka->getResponseCode()<<" "<<hlavicka->getResponseMessage()<<endl<<flush;
				exit (EXIT_FAILURE);
			}
			if ((hlavicka->getResponseCode()>=300)&&(hlavicka->getResponseCode()<400))
			{
				string novy_objekt = regexp(hlavicka->getHead(),"Location: ([^\n\r]+)");
				//cout << "---------- redirect " << novy_objekt<<endl<<flush;
				pocet_redirectu++;
				if (pocet_redirectu >= 5)
				{
					cerr << "Reached 5 redirections - " << hlavicka->getResponseCode() << " " << hlavicka->getResponseMessage()<<flush;
					delete hlavicka;
					exit (EXIT_FAILURE);
				}
				delete hlavicka;
				THead *hlavicka = new THead(novy_objekt);
			}

		}

		// kdyz mam ziskanou finalni hlavicku obektu, tak vypisu pozadovane info
		for (int i=0; i<=3; i++) // i - index akce
		{
			switch (parametry.getAction(i))
		  	{
				case UNDEFINED:
					break;
				case FULLHEAD:
				  	hlavicka->printFullHead();
				  	break;
				case OBJECT_LENGTH: 
				  	hlavicka->printObjectLength();
				  	break;
				case SERVER_IDENTIFICATION: 
				  	hlavicka->printServerIndentification();
				  	break;
				case LAST_MODIFICATION: 
				  	hlavicka->printLastModification();
				  	break;
				case OBJECT_TYPE:
				  	hlavicka->printObjectType();
				  	break;
				default:
					delete hlavicka;
					return EXIT_FAILURE;        
		  	}
		}
		delete hlavicka;
	}

	catch (TParams::badParameters)
	{
		cerr << "Spatne parametry" << endl;
		return EXIT_FAILURE;
	}
	
	catch (TParams::badActionIndex)
	{
		cerr << "Spatny index pro akci" << endl;
		return EXIT_FAILURE;
	}

	catch (THead::badHostName)
	{
		cerr << "Spatne zadana adresa" << endl;
		return EXIT_FAILURE;
	}
	
	catch (THead::socketError)
	{
		cerr << "Socket error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (THead::gethostbynameError)
	{
		cerr << "gethostbyname error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (THead::writeError)
	{
		cerr << "Write Error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (THead::readError)
	{
		cerr << "Read Error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (THead::closeError)
	{
		cerr << "Close Error" << endl;
		return EXIT_FAILURE;
	}
	catch (THead::noResponseNumberError)
	{
		cerr << "No Response number" << endl;
		return EXIT_FAILURE;
	}
	catch (THead::noResponseMessageError)
	{
		cerr << "No Response Messsage" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
